# client

An application and shell script to test the [routing-server](https://gitlab.com/macalimlim/routing-server)

## Setup

You need to have [leiningen](https://leiningen.org/) and [JDK](https://jdk.java.net/) installed

Clone this project
```sh
$ git clone https://gitlab.com/macalimlim/client
```

## Usage

```shell
$ lein run
```
or
```shell
$ ./do-post-requests.sh
```
