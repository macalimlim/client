#!/bin/sh

rm requests.log

for num in {1..3000}
do
    curl -s -X POST -H "Content-Type: application/json" -d '{"name": "mike", "email": "macalimlim@gmail.com"}' http://localhost:9000/route >> requests.log; echo -e "" >> requests.log
done

cat requests.log | grep 'localhost:3000' | wc -l
cat requests.log | grep 'localhost:3001' | wc -l
cat requests.log | grep 'localhost:3002' | wc -l
echo -e "---------"
cat requests.log | grep 'localhost' | wc -l
