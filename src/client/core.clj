(ns client.core
  (:require [clj-http.client :as http]
            [clojure.data.json :as json]
            [clojure.walk :as walk])
  (:gen-class))

(defn -main
  "I don't do a whole lot ... yet."
  [& _]
  (let [req  {:content-type "application/json"
              :body         (json/write-str {:name  "mike"
                                             :email "macalimlim@gmail.com"})}
        rss1 (pmap (fn [_] (http/post "http://localhost:9000/route" req)) (range 3000))
        rss  (map #(:host (walk/keywordize-keys (json/read-str (:body %)))) rss1)
        bss  (map #(:name (walk/keywordize-keys (json/read-str (:body %)))) rss1)
        bs   (count (filter #(= % "mike") bss))
        s1   (count (filter #(= % "localhost:3000") rss))
        s2   (count (filter #(= % "localhost:3001") rss))
        s3   (count (filter #(= % "localhost:3002") rss))]
    (println s1)
    (println s2)
    (println s3)
    (println "-------")
    (println bs)))
    ;; results may vary depending on the value current, number of requests and/or if a request is coming from a different source (process/thread)
    ;; (System/exit 1)))
